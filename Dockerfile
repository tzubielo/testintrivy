FROM alpine:3.11
LABEL maintainer="DevOps SaaS Team <ops@aquasec.com>"

RUN apk update && apk --no-cache add ca-certificates git rpm

WORKDIR /opt/aquasec
COPY bin/scanning-service-linux /opt/aquasec/scanner

ENTRYPOINT ["/opt/aquasec/scanner"]